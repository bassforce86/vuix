# VUIX (Vortex User Interface eXtension)
Vortex Skin


## Installation

Close Vortex before installing.

Open up "AppData\Roaming\Vortex\Themes".

Note: If you used Shared mode under Settings -> Vortex your themes folder will be C:\ProgramData\vortex\themes

Copy the folders of the colours you want into this directory.

Open Vortex and navigate to Settings -> Themes and select 'vuix'.


## Uninstallation

Ensure you change to a different theme than the one you wish to remove.

Close Vortex.

Open the themes folder.

Delete the vuix folder. 

Start Vortex.
